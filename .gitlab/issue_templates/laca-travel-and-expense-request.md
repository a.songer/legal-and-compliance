## Request Requirements

This template is to be used whenever a LACA team member wishes to use LACA budget, which may include conferences, events, and sponsorships, or the purchase of tools / software. 

1. Requests must be made no later than 90 days prior to the event.
2. LACA team members must complete this template to request approval from LACA leadership and Finance.
3. All travel must be booked in accordance with the [GitLab Travel and Expense Policy](https://handbook.gitlab.com/handbook/finance/expenses/). This includes booking all travel within GitLab's Travel and Expense platform, and adhering to stated limits with respect to flights, hotels, meal reimbursement, etc.

## Request Process

### Type of Request
- [ ] [Growth & Development Request](https://handbook.gitlab.com/handbook/total-rewards/benefits/) (This type of request requires the team member to submit a separate issue. If approved, the cost of the program will be covered by the G&D fund; however, travel & expenses may be covered by LACA.)
- [ ] Event / Conference
- [ ] Tools / Software

### Summary of request

1. Request title:
2. Cost:
3. Estimated cost of travel (if applicable):
4. Summarize value: [_Include information about how this impacts your role at GitLab, as well as how you can bring content back to GitLab and share._]
5. - [ ] Attach any relevant documentation (quote, proposal, order form, etc.)

### Approvals

#### LACA Approvals

- [ ] {Username Tag} Tag manager here for approval. Managers, check the box if approved.
- [ ] {Username Tag} Tag applicable CLO direct report for approval.
- [ ] {Username Tag} If approved by manager and CLO report, tag CLO for approval.

#### Finance Approvals

- [ ] {Username Tag} Once all LACA approvals have been received, tag FP&A manager, Cameron Smith, to approve budget.
