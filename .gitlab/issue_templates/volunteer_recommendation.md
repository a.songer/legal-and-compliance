## Volunteer Recommendation Template

Thanks for recommending a volunteer opportunity or nonprofit for consideration. Please provide the following details. The ESG Team will vet the nonprofit to ensure they [meet our criteria](https://handbook.gitlab.com/handbook/legal/philanthropy-policy/#who-we-support). Once verified, the nonprofit will be added to the volunteer list.

1. Is this opportunity virtual or in-person?
    - [ ] virtual
    - [ ] in-person


1. If in-person, what city, state/country is the nonprofit/opportunity?


1. Does this opportunity require a certain expertise? If yes, please indicate what type of expertise:
    - [ ] None
    - [ ] Technical/coding/AI
    - [ ] Legal
    - [ ] Finance
    - [ ] Marketing
    - [ ] Other:


1. What cause area does this opportunity fall into? Please feel free to add and include multiple options:
    - [ ] Animals
    - [ ] Aging
    - [ ] Children and youth
    - [ ] Cultural and education
    - [ ] Disabilities
    - [ ] Disaster relief
    - [ ] Environmental
    - [ ] Health
    - [ ] Homelessness
    - [ ] Hunger and poverty
    - [ ] Racial equity
    - [ ] Women
    - [ ] LGBTQ+


1. Does this nonprofit require training to volunteer? If so, please indicate how much training is required.


1. What is the name of the nonprofit you are recommending?


1. What is the mission of the nonprofit?


1. How can team members sign up to participate?


1. Anything else team members should know?



---
/confidential
/epic https://gitlab.com/groups/gitlab-com/-/epics/2370