# **Initial Issue template for global expansion legal process**

_**Welcome**_ to the global expansion legal process initial issue!  

_This issue should be used to start and manage the _initial_ legal review process of gathering, processing and assessing information when we look at opening, expanding or scaling a new country for hire/global expansion._

On receiving instruction to move ahead with a review/assessment of legal risk for opening or expanding a country for hire, a folder will be created for that country in the shared [Global expansion drive](https://drive.google.com/drive/folders/0ALzUKh4XsBAsUk9PVA?usp_dm=true) and an initial issue opened, using this template.

As information is gathered and assessed, if a decision is reached on a recommended solution to begin with, whether by PEO/EOR, entity/subsidiary or branch, _a further issue_ should be opened to manage the steps of progressing a review for that solution, in that country, based on the templates linked below:

- PEO/EOR issue template [here](https://gitlab.com/gitlab-com/legal-and-compliance/-/blob/master/.gitlab/issue_templates/global-expansion-legal-process-next-step-if-PEO-EOR-issue.md)
- Entity/subsidiary issue template [here](https://gitlab.com/gitlab-com/legal-and-compliance/-/blob/master/.gitlab/issue_templates/global-expansion-legal-process-next-step-if-entity-subsidiary-issue.md)
- Branch issue template [here](https://gitlab.com/gitlab-com/legal-and-compliance/-/blob/master/.gitlab/issue_templates/global-expansion-legal-process-next-step-if-branch-issue.md)

The full legal process/playbook for global expansion is set out [here](https://docs.google.com/document/d/1pw5ji3IuAgUiMIJLNPIOU_ADNHtYrAN0iOKXxJjlcAk/edit).

**Issue Creation Steps**

* [ ] 1. Appropriately title the issue according to the country being assessed using this format: Global expansion - legal review process - initial issue - COUNTRY NAME

* [ ] 2. Please mark this issue as confidential and assign to:
    * [ ] - Emily Plotkin and Sarah Rogers
    * [ ] - Darren Burr (EMEA) _OR_ Tara Kumpf (APAC) (depending on the location of the country)
    * [ ] - Rashmi Chachra
    * [ ] - Lynsey Sayers
    * [ ] - Matthew Taylor
    * [ ] - Harley Devlin

* [ ] 3. Add appropriate labels (global expansion legal process labels are in green)

* [ ] 4. Add a link to the matter folder for the country in the global expansion drive [here].

* [ ] 5. Add a due date _(discuss with Emily whether this is appropriate or necessary on a case-by-case basis)_

**Preliminary step where we already have a presence in the country:**

Gather current status information with [Employment Solutions Partner](@hdevlin) which can mostly be gathered from the [Country list (current hiring)](https://docs.google.com/spreadsheets/d/1rH8Afcnp38knsnchJtm2iPAzCSO8qxsum-fkdNFspRE/edit#gid=1223524307), the [Current database & road map for Business Expansion](https://docs.google.com/spreadsheets/d/1eotWhWgMZLuoUjLtv2aqYf28uwxrjFnfEGp3n9RxVR0/edit#gid=0) and the [Global Expansion Strategy - Int'l Expansion Team Considerations](https://docs.google.com/spreadsheets/d/1L8A_tUIo2x6Kf-H01Rm6zcU1lfWB1y01puxZl6jwMzI/edit#gid=0) and inputted to a new copy of [this document](https://docs.google.com/document/d/1QPulaTO9R1qISFM9uAL1LHybQaqhukgmJgk0j-5vDKA/edit).

## Initial Steps for Legal
### 1. Inform CLO Staff
* [ ] Send an initial email to each legal team director (with a link to this issue so they may follow progress and provide feedback), using the templates [here](https://gitlab.com/gitlab-com/legal-and-compliance/-/issues/1027), requesting the information set out in the templates and confirmation of any questions they wish to have included in a request to counsel for a fee estimate.

* [ ] Rashmi Chachra - Corporate 
* [ ] Lynsey Sayers - Product and Privacy
* [ ] Matthew Taylor - Commercial
* [ ] Emily Plotkin - Employment

### 2. Engage Counsel
* [ ] Engage counsel with a presence in the location and request cost/fee estimates. There is a template for use [here](https://gitlab.com/gitlab-com/legal-and-compliance/-/issues/1028). [Save estimates etc to country folder for good matter management]
* [ ]   Track costs for accruals in [this spreadsheet](https://docs.google.com/spreadsheets/d/1Gpzik5UJUck9mDK3etL92xJbNQRkkLawgqyr87_A07E/edit#gid=0) 

## Research and Discovery

* [ ] Assess advice from counsel
Follow up with:
* [ ] - Emily Plotkin 
* [ ] - Darren Burr (EMEA) _OR_ Tara Kumpf (APAC) (depending on the location of the country)
* [ ] - Rashmi Chachra
* [ ] - Lynsey Sayers
* [ ] - Matthew Taylor
to ensure advice from counsel has been received and reviewed

## Assessment/Decision-Making

### A. First step: What is the recommended employment solution to begin with?

_i. PEO/Employer of Record_

1. If considering PEO/Employer of Record, do the laws of the jurisdiction recognize this type of employment solution? 
2. Is there a threshold number of employees employed by PEO/EOR before the company should move to a different solution?
3. Does a PEO support equity in this location?
4. Does a PEO support all GitLab roles in this location (Sales, including PubSec, R&D, Support, G&A, Marketing)
5. Is this a location where we are trying to reach Public Sector clients?  If so, can a PEO support PubSec?
6. Time to set up PEO in location

_ii. Branch_

1. Is there a threshold number of employees for this option?
2. Are there types of employment roles (Sales, including PubSec, R&D, Support, G&A, Marketing) that this option does not support?
3. Can a branch support a Public Sector role?
4. What is the risk of permanent establishment?
5. Can a branch support equity?  What effect does it have on the foreign entity serving as the employer?
6. What does the foreign entity need to do to register to employ directly in the location?
7. Are the compliance requirements the same as if the company had a direct entity in this location?
8. Costs of a branch
9. Time to set up branch in location

_iii. Entity/Subsidiary_

1. Reasoning for moving directly to entity
2. Compliance requirements (see below)
3. Equity implications
4. Costs of creation
5. Time to set up entity

#### Once a decision has been reached has been made on a likely solution to begin with, note the decision below and manage by the further steps of progressing a review for that solution, in that country, by:
* [ ] opening a separate 'next step' issue using one of the relevant templates listed above (either [for PEO/EOR](https://gitlab.com/gitlab-com/legal-and-compliance/-/blob/master/.gitlab/issue_templates/global-expansion-legal-process-next-step-if-PEO-EOR-issue.md), [entity/subsidiary](https://gitlab.com/gitlab-com/legal-and-compliance/-/blob/master/.gitlab/issue_templates/global-expansion-legal-process-next-step-if-entity-subsidiary-issue.md) or [branch](https://gitlab.com/gitlab-com/legal-and-compliance/-/blob/master/.gitlab/issue_templates/global-expansion-legal-process-next-step-if-branch-issue.md)); and 
* [ ] cross-referencing the new 'next step' issue in this issue, just below (and this initial phase issue in the next step oneone).

##  **B. Decision:**
A decision was made that a likely solution to begin with will be (delete as relevant)[use a PEO/to move to create an entity or subsidiary/to open a branch] and a separate issue for managing the next step of the legal process for [country] is [here]****

## Final steps

* [ ] Once the 'next step issue is complete/closed',return to this issue to remove the 'Global expansion legal process: underway' label (which will automatically apply the 'Global expansion legal process: complete' label); and 
* [ ] close this initial issue.

/confidential
