<!--Please note that this issue template should be used for escaltions related to requests to [`Subscription Owner changes`] (https://handbook.gitlab.com/handbook/support/license-and-renewals/workflows/customersdot/associating_purchases/#ownership-verification) in the CustomerDot system.--> 

### Customer information  
- Customer organization name:
- Customer email domain name:
- Customer GitLab group URL:
- Request Zendesk ticket link:  

### Current subscription owner information 
- **Sold to** contact information:
- Email address:
- Customer Billing Account:(https://customers.gitlab.com/admins/sign_in)

### Please confirm all self-service options been exhausted.
- `[Confirmed]` 

### Requestor information
- Name:
- Email address:
- Is the requestor the **Bill to** contact?
- Current role held within this group:
- Position held at customer organization, if known:

### New Owner information (if different from Requestor information)
- Name:
- Email address:
- Is the new owner the **Bill to** contact?
- Position held at customer organization, if known:

### What has already been done in an attempt to verify subscription ownership? Please include a brief explanation as to why the customer is unable to meet the account [`ownership verification requirements`] (https://handbook.gitlab.com/handbook/support/license-and-renewals/workflows/customersdot/associating_purchases/#ownership-verification).

### Additional information
- Timing for the resolution of this request:
- Any other information relevant to this request (example(s): Did the customer purchase the subscription via a third party reseller, has the sales team reached out on behalf of the customer):

<!--Do not edit below this line-->
/confidential
/label ~"Subscription Ownership Change"
