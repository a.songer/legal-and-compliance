# New GitLab Policy and Policy Amendment Requests

<!-- NOTE: PLEASE LEAVE THIS ISSUE UNASSIGNED - THE LEGAL TEAM WILL REVIEW AND ASSIGN ON OUR END -->
<!-- NOTE: DO NOT DELETE ANY OF THE SECTIONS OF THIS ISSUE. THEY ARE REQUIRED FOR COMPLIANCE -->

## Purpose

This issue template is designed to collect, facilitate, and document approvals needed to implement new policies and/or amend certain existing policies. Such approvals may include but are not limited to approval from GitLab’s Chief Legal Officer, Chief Financial Officer, and/or the Board of Directors.
Approval must be solicited and obtained using this template for:

- any new policy (see definitions below); and,
- amendments to any policy, procedure, or guidelines identified here (see Tab 2, [Policy List](https://docs.google.com/spreadsheets/d/1tY5LEMsB8r7T4bcL7ZnBo5nuyyIg_ptO047hTlmAqYA/edit#gid=728066571)).  

Note that DRIs for both new and existing policies, procedures, and guidelines identified in the Policy List are responsible for soliciting input from cross-functional stakeholders prior to submission of their request. All approvals, including approval by the Board of Directors, if necessary, must be obtained prior to the new policy or amended policy’s publication. 

Requests will be acknowledged within three business days. Processing times will vary depending on various factors. New policies or amendments requiring approval from the Board of Directors will be approved no earlier than the next scheduled quarterly board meeting. 

## Objective

- Please provide a link to the draft policy/amendment.
- Please explain why the policy/amendment is needed. 

## Additional Information

Please complete all the fields. Any that are not relevant to your request should be marked as “N/A.”

## Amendments to Existing Policy

Answer these questions/provide this information, if you want to amend an existing policy:

1. Have you identified all relevant stakeholders and obtained their input on the draft amendment? 

- [ ] Yes 
- [ ] No

If the answer is “yes,” identify those stakeholders here: @personshandle

2. Are you the DRI for this policy (see Column B of the Policy List)? 

- [ ] Yes
- [ ] No

If the answer is “no,” solicit the DRI’s approval here: 
DRI approval: [insert @peopleshandle here]

3. Summarize your proposed amendment: 

4. Identify any laws or regulations relevant to this amendment:

5. Is there an urgent need for this amendment? 

- [ ] Yes 
- [ ] No

If the answer is “yes,” (a) explain the need and (b) identify any relevant dates, deadlines, etc:

## New Policy

Answer these questions/provide this information, if you are requesting a new policy: 

1. Have you identified stakeholders and obtained their input on this draft? 

- [ ] Yes 
- [ ] No

If the answer is “yes,” identify those stakeholders here: @peopleshandle

2. Are you the appropriate DRI for this draft policy? 

- [ ] Yes
- [ ] No

If the answer is “no,” solicit approval from the appropriate DRI here: 
DRI Approval: [insert @peopleshandle here]

3. Explain the policy’s purpose and scope:

4. Identify any relevant laws or regulations:

5. Explain where the policy will reside in the handbook (including a link if possible).

6. Will this policy overlap in whole or in part with an existing policy or procedure? If the answer is “yes,” please (a) identify and provide a link to the existing policy or procedure and (b) explain the overlap.

7. Will this policy complement, support, or be used in conjunction with an existing policy or procedure? If yes, please provide a link to that policy.

## Definitions

**Policy**: An official statement expressing GitLab’s position on an issue of company-wide importance. Policies typically have broad application, mandate or constrain actions, establish rights or obligations, and/or guide the decisions and actions of GitLab and its team members, and exist to ensure compliance with applicable laws, regulations, and organizational standards, or to promote ethical standards.

**Procedure**: Establish how team members should carry out policy requirements. Procedures typically provide step-by-step instructions.

**Guidelines**: Additional information used to support or help team members interpret policies and procedures. 

**Stakeholder**: Team members with expertise in the policy’s subject matter or whose operations will be significantly affected by the policy.

Confirm that you have done the following prior to submitting this issue request:

- [ ] Titled this issue either (1) New GitLab Policy: [insert proposed policy name or subject], or (2) Proposed Amendment to GitLab Policy [insert existing policy name]
- [ ] Solicited and obtained input and approval from cross-functional stakeholders
- [ ] For amendments to existing policies, solicited and obtained approval from the relevant DRI (if you are not the DRI yourself);
- [ ] Included a link to the draft policy or policy amendment (with tracked changes)
- [ ] Assigned the issue to yourself, the DRI (if you are not the DRI yourself).

**Do not edit below**

/assign @rachelpack

/confidential

/label ~E&C::New ~Change Management Policy Request 
