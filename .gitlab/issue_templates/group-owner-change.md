<!--This template must be populated for all group owner change requests requiring legal review--> 

### Group information  
- Customer organization name:
- Customer email domain name:
- Customer GitLab group URL:
- Request Zendesk ticket link:  

### Current owner information 
- GitLab username:
- Email address:
- Date user last active:
- Position held at customer organization, if known:

### Have self-service options been exhausted?
- `Yes`/`No` 

### Requestor information
- GitLab username:
- Email address:
- Current role held within this group:
- Position held at customer organization, if known:

### Has the requestor provided a copy of their most recent GitLab invoice?
- `Yes`/`No` 
<!--If Yes, upload the invoice provided by the customer here-->

### Has the requestor provided a complete and correctly populated version of the PDF referred to in the [`Support::SaaS::Account ownership change verification (Self-service option not possible)`](https://gitlab.com/search?utf8=%E2%9C%93&group_id=2573624&project_id=17008590&scope=&search_code=true&snippets=false&repository_ref=master&nav_source=navbar&search=id%3A+360073396100) macro?
- `Yes`/`No` 
<!--If Yes, upload the completed PDF here-->

### Additional information
- Timing for the resolution of this request:
<!--State the timing for this request, including whether there is any urgency-->

- Any other information relevant to this request, including indicating whether the requestor mentioned any internal dispute or employee termination:
<!--Add any other information that is relevant to this request-->

<!--Do not edit below this line-->
/confidential
/label ~"Group Owner Change"
