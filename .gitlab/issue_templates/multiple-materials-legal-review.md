<!-- Refer to the multiple materials legal review process 
(about.gitlab.com/handbook/legal/materials-legal-review-process/#track-2-multiple-materials-legal-review-process) and complete this template to obtain legal review of multiple pieces of material with a related purpose, like several slide decks being prepared for one event. If you need to obtain a legal review of a single piece of material, complete the single material legal review issue template instead. -->

<!-- Title the issue as follows: “[name of event]_Materials Legal Review”, for example “Partner SKO 2022_Materials Legal Review”-->

### For the DRI: 
Provide the following general information regarding the event or other purpose.

#### 1. Name, date, and brief description of event or other purpose:
<!-- If the purpose for which the materials are being created is an event, state the name, date, and a brief description of the event here. If the purpose is something other than an event, state and briefly describe that purpose here and indicate any relevant dates. -->

#### 2. Is the event or other purpose for these materials internal or external?
<!-- Delete as appropriate, and refer to the definitions of `external use` and `internal use` in the Materials Legal Review Process. If necessary, given further details If there are plans to use the material, or any part of it, externally in the future, or the event or other purpose will involve both internal and external materials, chose `external`. -->
- external @sfriss @LeeFalc
- internal @sfriss

#### 3. Will the materials be made available on GitLab Unfiltered, Edcast, or anywhere else?
<!-- Delete as appropriate to state whether some or all of the materials being submitted for review will be made available anywhere. If they will, give details of the visibility the materials will have. -->
- yes <!-- if yes, give details -->
- no 

#### 4. Materials to be reviewed:
<!-- Provide a general description of the types of items that will need to be reviewed, e.g., presentation decks, scripts, videos, etc.-->

#### 5. Due date for review:
<!-- State the due date for review, and indicate this as the due date of the issue below, noting that:<br>
1. For multiple materials legal review requests involving five or fewer pieces of material, legal aims to complete the review within 5 business days of submission; and 
2. For multiple materials legal review requests involving more than five pieces of material, the DRI must, no less than 5 business days before the materials will be ready for review, (i) notify the Legal & Corporate Affairs Team in #legal of the upcoming event, and (ii) arrange a sync with the Team to agree a timeline for the completion of the review. -->

----

<!-- do not edit below - the following information should stay in the issue description for the materials creators to review when submitting materials  -->

### For the Materials Creators: 
When creating materials:
- for internal use, follow the [SAFE Framework](https://about.gitlab.com/handbook/legal/safe-framework/); and
- for external use, follow the [SAFE Framework](https://about.gitlab.com/handbook/legal/safe-framework/) and the [Guidelines for Use of Third-party IP in External Materials](https://about.gitlab.com/handbook/legal/ip-public-materials-guidelines/).

Definitions of `external use` and `internal use` are set out in the [Materials Legal Review Process](https://about.gitlab.com/handbook/legal/materials-legal-review-process/#external-vs-internal-use).

Link (for Google Docs) or upload (for other file types) the material for review in individual comment threads within this issue so that feedback for each piece of material can be captured separately. Additional material for review can be added in new comment threads.

When adding materials to this issue for review, respond to the following questions in the individual comment threads for each piece of material.

#### 1. Is the material subject to mandatory review? Material subject to mandatory review must be reviewed from a SAFE perspective AND an IP perspective. If unsure, refer to the [definition of mandatory review](https://about.gitlab.com/handbook/legal/materials-legal-review-process/#mandatory-review).
 - If yes, identify the type of material from the list of materials subject to mandatory review and indicate that in the individual comment thread related to that piece of material.
 - If no, skip to the next question. 

#### 2. Does the material comply with the [SAFE Framework](https://about.gitlab.com/handbook/legal/safe-framework/) and the [Guidelines for Use of Third-party IP in External Materials](https://about.gitlab.com/handbook/legal/ip-public-materials-guidelines/)?
 - Indicate in the individual comment thread if the material does not comply with either or both of the SAFE Framework OR the IP Guidelines.
 - If you're not sure, indicate as such. 

#### 3. If the material is NOT subject to mandatory review and complies with the SAFE Framework AND the IP Guidelines, legal review is not mandatory. However, if you have specific questions regarding the material, set those out in the comment thread pertaining to that specific material and identify the aspect(s) of the material to which the questions relate.

/confidential
