### READ THESE DIRECTIONS BEFORE SUBMITTING

The purpose of this intake template is to address RFP, RFI or RFX questions which cannot be found via the following:
- Questions related to deal / opportunity structure (including discount(s)) should be handed by [Deal Desk](https://about.gitlab.com/handbook/sales/field-operations/sales-operations/deal-desk/#communicating-with-the-deal-desk-team) in SFDC.
- For questions related to ESG, Legal, People, and Security, please review the details in [AnswerBase](https://app.vendorpedia.com/vendor-portal/libraries). 

### Please confirm that you have done the following prior to submitting this issue request: 

- [ ] Confirm you have read the instructions above, and have checked available resources to address questions. Additional details can be found in the [RFP Process Handbook](https://about.gitlab.com/handbook/security/security-assurance/field-security/Field-Security-RFP.html#request-for-proposal-rfp-process).
- [ ] Confirm you have ASM or above approval with respect to the RFP, RFI or RFX. 

### Content Area
<!-- Please select which content areas require attention. DELETE OPTIONS NOT SELECTED to avoid unnecessarily tagging individuals. -->

- [ ] Security (@gitlab-com/gl-security/security-assurance/governance-and-field-security/field-security-team)
- [ ] ESG (@slcline, @kbuncle)
- [ ] Commercial Legal (@m_taylor, @jbraughler)
- [ ] Privacy Legal (@emccrann, @bronwynbarnett)
- [ ] Corporate Legal (@sfriss, @rchachra )
- [ ] Product (@hbenson)
- [ ] Finance (@lwhelihan, @andrew_murray)
- [ ] People (@cgudgenov)
- [ ] Product Marketing (@nicolecsmith)

### Background Details
<!-- Complete each line item below. -->

| RFP, RFI, or RFX-related Question| Answer/Link(s)|
|--------------------------|--------------------|
| GitLab product (GitLab Dedicated, GitLab.com, GitLab Self Managed, FedRamp, Other)|                          |
| ARR amount (in USD)|                           |
| SFDC Opportunity (Link)|                   |
| ANY AND ALL DOCUMENTATION (Attachments) |                   |

### Opportunity Details

**At a high level, describe this Opportunity for GitLab and the question(s) that need attention**




---
---
### Do Not Edit Below


/label ~"RFP::triage"
/confidential

@gitlab-com/gl-security/security-assurance/field-security-team

---
