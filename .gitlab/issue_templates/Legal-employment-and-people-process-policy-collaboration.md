_This template should be used when the People team need a review by legal, employment of an employment 'people' policy_

Using this issue and applying the correct label will add the issue directly to the [Legal Employment Issue Board](https://gitlab.com/groups/gitlab-com/-/boards/4585701) where it will be picked up and triaged. The earlier legal, employment are included, the better we can support a request.

## **Issue Creation Steps**

Please:
* [ ] 1. title the issue using this format: People policy - legal employment review request;

* [ ] 2. include: the details of the policy which requires review (with a link which legal, employment can acces to edit), whether the policy is new or an update to an existing policy, and whether the policy applies globally, by region, or is entity specific (if there's a timeline within which the rewiew is required, please also include this with the request);

* [ ] 3. mark this issue as confidential and add the labels 'legal-employment-policy' and `legal-employment::to-do`; and

* [ ] 4. assign the issue to:

    * [ ] - Emily Plotkin
    * [ ] - Sarah Rogers
    * [ ] - Hayley Hobbes
    
     and: 
     * [ ] - Darren Burr (EMEA) _OR_ 
     * [ ] - Tara Kumpf (APAC) 
     (if there's a regional element to the policy which requires review.)

     /confidential
