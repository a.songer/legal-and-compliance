## Vendor Procurement Privacy Review

_This assessment is a requirement that should be performed as part of the [Vendor Contracts - Procure to Pay process](https://about.gitlab.com/handbook/finance/procurement/). The Product Manager or the System Admin has the responsibility and accountability for ensuring submission and completion of the Privacy Review.  The admin on the respective technology should sign off on the Privacy Reivew to signify understanding and accountability for the risks in the particular technology.  Product Managers/Admins should ensure that the DPO and Privacy Officer are consulted, in a timely manner, in all issues relating to the protection of personal data. DPOs can delegate DPIA assessments to Security Compliance Analysts where appropriate._

For additional information about Privacy Review requirements, please consult the:

* [Vendor Privacy Review Process](/handbook/legal/privacy/#privacy-review-process)

## Requestor to Complete

### Step 1 - Name Issue

- [ ] Name the issue as follows: Software Name - Privacy Review - Business Owner Name

### Step 2 - Link Issues

<!--(Link the following in the "Related Issues" box, not in issue description or in comments)-->

- [ ] Procurement issue 
- [ ] Security Review issue

### Step 3 - Link Relevant Documentation

- [ ] Link vendor's Privacy Policy here: 

### Step 4 - Answer the Following Questions

- **Is this tool On-Premise or SaaS?**

- **Mark data subjects**
<!--Mark one or both-->
  - [ ] GitLab Team Members
  - [ ] Customers
  - [ ] Prospects/Leads

- **What Personal Data will be used to create team members' accounts?** *(Note: names and email addresses used for log-in are Personal Data)*

- **What specific fields of Personal Data related to the data subjects will be collected, shared, or processed by this tool?** 
<!--List out the specific field names for the data that will be collected/shared/processed. For a definition of "Personal Data," please review [Legal's Privacy Glossary](https://about.gitlab.com/handbook/legal/privacy/#privacy-glossary)-->

- **Are there in-tool data deletion capabilities?**
<!--Please note whether we will be able to delete data subjects from the tool ourselves or if we will need to contact the vendor to do so. Also note if there are any auto-deletion settings that we can utilize (ex. Slack messages auto-delete after 90 days)-->


- **What does this software do?** 
<!--Describe in non-technical terms what GitLab proposes to use this software for and how it would be incorporated into our existing Tech Stack. Include justification for why any existing tools in the tech stack cannot be used for the same purpose-->


- **Explain the benefit to GitLab in using this software**
<!-- Describe in non-technical terms how this software contributes to the achievment of company goals.-->

- **Will this software be used in any way for targeted advertising?** 
<-- Describe how/where in the marketing stage, this tool may be used in furtherance of GitLab targeted advertising efforts. For instance, if the software will compile audiences, or the software will be used to generate emails, or the software will be used to capture contact information from leads/prospects to be added to another-->


---
---

## Privacy Intake to Complete (with Requestor's Assistance)

**1a. Is any additional Personal Data collected, shared or processed?**
<!--List separately the following: data collected while GitLab team members use the tool, additional data stored in the tool for GitLab purposes-->

**1b. Is there any sensitive data involved?**

**2.  If Personal Data resides in the tool, what is the retention period?**  

**3.	Does Vendor wish to retain access to, or use the Gitlab data for any purposes of their own?**  *e.g. analytics, system improvements*

**4.  Is this a new or modified processing activity?** 

**5.  Within the context of the processing activity, what is the relationship between the Controller and consumer and the consumer's reasonable expectations?**

**6.  Does the use of this software constitute a sale or share of personal data?**

**7.  Is this activity processing personal data for profiling?**
      - [ ] Yes  
      - [ ] No 
---
---

## Privacy Officer to Complete 

| Risk   | Remediation |
| ------ | ------ |
| If sensitive data is processed, the data must be deleted within 24 hours | _describe process for this remediation_ |
| For any identified risks to the rights of consumers, there should be measures and safeguards to reduce that risk | _describe remediation measures to be put into place to reduce risk (de-identified data, data security practices, etc)_ | 
| describe risk | describe remediation |
| describe risk | describe remediation |


**DPA Required?**
  - [ ]  Yes
  - [ ]  No

**Add to Personal Data Request template?**
  - [ ]  Yes
  - [ ]  No

**Subprocessor?**
  - [ ]  Yes
  - [ ]  No
---
---

## Approval

- [ ]  Yes, with no further review
- [ ]  No
- [ ]  Yes, but further privacy review required upon implementation (Notify Business Owner)

<!--Do not edit below this line-->
-----

/confidential
/label ~"Privacy Vend Rev::Intake"
/label ~Privacy
